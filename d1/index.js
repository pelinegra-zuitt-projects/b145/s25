// CRUD Operations
//Create
//- to insert documents

db.users.insertOne({document})
db.users.insertOne(
	{
		"firstName": "Jane",
		"lastName": "Doe",
		"age": 21,
		"contact": {
			"phone": "87666665",
			"email": "janedoe@gmail.com"
		}
		"courses": ["CSS", "Javascript", "Python"],
		"department": "none"
	}	
);

db.collections.insertMany(
	[
		{
			"firstName":"Stephen",
			"lastName":"Hawking",
			"age":76,
			"contact":{
				"phone": "233443",
				"email":"stephenhawking@gmail.com"
			},
			"courses": ["Python","React","PHP"],
			"department": "none"
		},
			{
			"firstName":"Hanna",
			"lastName":"Lowe",
			"age":34,
			"contact":{
				"phone": "235443",
				"email":"hannalowe@gmail.com"
			},
			"courses": ["Python","React","PHP"],
			"department": "none"
		}
	]

);

//Read operation
//-retrieves documents from the collection

// db.collection.find({query},{field projection})

//find() method , if parameter is empty, it retrieves everything


//Update Operations
// updateOne()
// updateMany()
// replaceOne()

db.collections.updateOne({filter}, {update})
db.collections.updateMany()

//upaateOne() ,method
db.users.insertOne(
	{	

		{
			"firstName":"Test",
			"lastName":"Test",
			"age":0,
			"contact":{
				"phone": "0",
				"email":"test@gmail.com"
			},
			"courses": [],
			"department": "none"
			"status":"active"
		}
	}

);

//modifying the added doc using updateOne() method


//use firstname field as a filter and look for the name test
//Using update operator set, update the fields of the matching document with the following details
//bill gates, 65years old,  phone 12345678, email bill@email.com courses PHP,laravel, html, operations department  status:active

//modifying the added doc using updateOne() method
db.users.updateOne(
	{ _id: ObjectId("61ed07b692cad282397ca150")},
	{ $set:
		{
		"firstName":"Bill",
		"lastName":"Gates",
		"age":65,
		"contact":{
				"phone": "12345678",
				"email":"bill@email.com"
				},
		"courses": ["PHP","Laravel","HTML"],
		"department": "operations"
		}
	}
);



db.users.updateOne(
	{ _id: ObjectId("61ed07b692cad282397ca150")},
	{ $set:
		{
		"status":"active"
		}
	}
);

//Update Many
//Department:None --> HR
db.users.updateMany(
	{ "department":"none"},
	{ $set: {
		"department": "HR"
		}
	}
)


//replaceOne()
//replaces whole document


//Miniactivity
// look for document  with a field status using object ID as filter
//remove the status field using update operator

db.users.updateOne(
	{ _id: ObjectId("61ed07b692cad282397ca150")},
	{ $unset:
		{
		"status":"active"
		}
	}
);



//Delete Operation
	//-delete a document/s
	db.collections.deleteOne(filter)
	db.collections.deleteMany()

	//insert a document as an example to be deleted
	db.users.insertOne({"firstName":"Joy","lastName": "Pague"})

	db.users.deleteOne(
		{"firstName":"Joy"}
	);


//insert a document as an example to be deleted
db.users.insertOne({"firstName":"Bill","lastName":"Crawford"})

//deleteMany()

db.users.deleteMany("firstName":"Bill")

