//Insert a single room (insertOne method)

db.hotel.insertOne(
		{
			name:"single",
			accomodates: 2,
			price:1000,
			description:"A simple room with all the basic necessities",
			rooms_available: 10,
			isAvailable: false
		}
);

//Insert Multiple rooms (insertMany method)

db.hotel.insertMany(
	[
		{
			name:"double",
			accomodates: 3,
			price:2000,
			description:"A room fit for a small family going on a vacation",
			rooms_available: 5,
			isAvailable: false
		},
		{
			name:"queen",
			accomodates: 5,
			price:3000,
			description:"A room fit for a family of 5",
			rooms_available: 2,
			isAvailable: true
		}

	]

);

//Use find method to search for a room with the name double
db.hotel.find({"name":"double"})



//Use the updateOne method to update the queen room and set the available rooms to 0
db.hotel.updateOne(
	{"name":"queen"},
	{ $set:
		{
		"rooms_available": 0
		}
	}
);


//Use the deleteMany method rooms to delete all rooms that have 0 availability

db.hotel.deleteMany({"rooms_available":0})